!/bin/bash
URL=${1:-$URL}

echo "Waiting for server to be ready"
sleep 3

echo "Test for node";
count=0
echo "Testing url $URL";

#loop that increment 3 to the counter $count
for i in 1 2 3
do
incrementeping=$(curl $URL/ping)
let count++
done
pingCount=$(curl $URL/count | jq .pingCount)

#test if compteur $count equal $pingCount recover to jq and curl request
if [ $count -eq $pingCount ]
then
	echo "Fonctionne :" $pingCount" = "$count
else
	echo "Fonctionne pas :" $pingCount" != "$count
fi
