# ping-pong-count app

Name : Thibaud Saint-Etienne & Lucas Reynes

Techno : nodejs

Clone the project
-------------------
```
git clone https://github.com/ThibaudSE/ping-pong-count.git
```

Change directory
-------------------
```
cd ping-pong-count
```

Install dependencies
-------------------
```
npm install
```

Start the server
-------------------
```
npm run start
```

Launch test
-------------------
```
./test.sh
```
