var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

var Raven = require('raven');
Raven.config('https://34f8123beaf74894bdfd83c7ef2bb0f0:0c4d6bab01334f848b517765dc1beae2@sentry.io/258367').install();

// The count var we use to show in the count route
count = 0; // test

// Define the ping route
app.get('/ping', function(req, res) {
    count++;
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ "message": "pong" }));
});

// Define the count route
app.get('/count', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ "pingCount": count }));
});

// Start the listening
app.listen(port);
console.log('Ping pong count server start on: ' + port);
Raven.captureMessage('Ping pong count server start');


// Start reading from stdin so we don't exit.
process.stdin.resume();

// On server stop (Ctrl + C)
process.on('SIGINT', function () {
    Raven.captureMessage("L'application s'est terminée");
});

//do something when app is closing
process.on('exit', function () {
    Raven.captureMessage("L'application s'est terminée");
});

//catches ctrl+c event
process.on('SIGINT', function () {
    Raven.captureMessage("L'application a été quitée");
});

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', function () {
    Raven.captureMessage("Kill pid exception");
});
process.on('SIGUSR2', function () {
    Raven.captureMessage("Kill pid exception");
});

//catches uncaught exceptions
process.on('uncaughtException', function () {
    Raven.captureMessage("Une erreur a fermé l'application");
});
